PRTG Device Template for AdTran AOS devices
===========================================

This project contains all the files necessary to integrate the AdTran OS
series switches into PRTG for auto discovery and sensor creation.

PRTG Device Templates
Installation Instructions
=========================

The template project has a standard directory structure:
All the files in the PRTG subdirectory needs to go into the PRTG program directory
(https://kb.paessler.com/en/topic/463-how-and-where-does-prtg-store-its-data).
The other files are for documentation and testing.

<pre>
Tested with :
1.3.6.1.2.1.1.1.0 = "Total Access 908 (2nd Gen), Version: A2.06.00.E, Date: Mon Feb 15 10:36:45 2010"
1.3.6.1.2.1.1.1.0 = "NetVanta 1234 PoE, Version: 18.01.02.00, Date: Wed May 25 14:04:29 2011"
</pre>
