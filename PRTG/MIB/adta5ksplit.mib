    ADTRAN-TA5KSPLIT-MIB
    DEFINITIONS ::= BEGIN

    -- TITLE:     The ADTRAN ANSI SPLITTER MIB
    -- PRODUCT:   1187105L1
    -- VERSION:   1.0
    -- DATE:      8/08/05

    -- This MIB defines the object identifier (OID) for the TA5K ANSI Splitter.

    --  SUPPORTED MIBS

    --  1.  ADTRAN-MIB
    --  2.  ADTRAN-GENSLOT-MIB
    --  3.  ADTRAN-GENPORT-MIB


    --   ADTRAN generic accessory (genaccessory)

    --  adGenAccessoryFaceplate (genaccessory)
    --  The first byte will be the number of LED/switches encoded.
    --  The following bytes will contain 2 bit sequences in network order
    --  for each LED or switch, from the top to the bottom of the faceplate.
    --  The final byte may be padded with zeros.
    --  The meaning of each pair of bits for LEDs is:
    --  00 - off
    --  01 - Green
    --  10 - Red
    --  11 - Orange (both colors on)
    --  LED order & bit position: PWR Led(7,6),TEST(5,4), TD(3,2), RD(1,0)

    -- HISTORY:
       -- 12/03/03 glf First edition

    IMPORTS
        TRAP-TYPE
            FROM RFC-1215
        enterprises
            FROM RFC1155-SMI
        OBJECT-TYPE
            FROM RFC-1212
        DisplayString
            FROM RFC1213-MIB
        adProducts,
        adMgmt
            FROM ADTRAN-MIB
        adGenSlotInfoIndex
            FROM ADTRAN-GENSLOT-MIB;

    -- OBJECT IDENTIFIERS
    --adtran                      OBJECT IDENTIFIER ::=  { enterprises 664     }
    --adProducts                  OBJECT IDENTIFIER ::=  { adtran 1           }
    --adMgmt                      OBJECT IDENTIFIER ::=  { adtran 2            }
    adTA5kSplitmg           OBJECT IDENTIFIER ::=  { adMgmt 755          }
    adTA5kSplit             OBJECT IDENTIFIER ::=  { adProducts 755     }
    --adTA5kSplitProv         OBJECT IDENTIFIER ::=  { adTA5kSplitmg 1 }
    adTA5kSplitStat         OBJECT IDENTIFIER ::=  { adTA5kSplitmg 1 }

--********************************************************************************************
    adTA5kSplitStatusTable OBJECT-TYPE
        SYNTAX  SEQUENCE OF AdTA5kSplitStatusEntry
        ACCESS  not-accessible
        STATUS  mandatory
        DESCRIPTION
            "Status table."
        ::= { adTA5kSplitStat 1 }

    adTA5kSplitStatusEntry  OBJECT-TYPE
        SYNTAX  AdTA5kSplitStatusEntry
        ACCESS  not-accessible
        STATUS  mandatory
        DESCRIPTION
            "An entry in Status Table."
        -- INDEX   { adGenSlotInfoIndex, adGenPortInfoIndex }
        INDEX   { adGenSlotInfoIndex }
               ::= { adTA5kSplitStatusTable 1 }

    AdTA5kSplitStatusEntry ::= SEQUENCE {
        -- Port-level status items
             adTA5kSplitTestType    INTEGER,
             adTA5kSplitTestBus     INTEGER,
             adTA5kSplitTestPort    INTEGER
        }

    adTA5kSplitTestType OBJECT-TYPE
                SYNTAX  INTEGER {
                                inactive(1),
                                monitor(2),
                                fullsplit(3)
                                }
                ACCESS  read-only
                STATUS  mandatory
                DESCRIPTION
                "This item indicates the type of test performed
                1 = Inactive
                2 = Monitor
                3 = Full Split"
        ::= { adTA5kSplitStatusEntry 1 }

    adTA5kSplitTestBus OBJECT-TYPE
                SYNTAX  INTEGER {
                                noaccess(1),
                                tr(2),
                                t1r1(3)
                                }
                ACCESS  read-only
                STATUS  mandatory
                DESCRIPTION
                "This item indicates the test bus used.
                1 = No Access
                2 = T/R
                3 = T1/R1"
        ::= { adTA5kSplitStatusEntry 2 }

    adTA5kSplitTestPort OBJECT-TYPE
                SYNTAX  INTEGER (0..32)
                ACCESS  read-only
                STATUS  mandatory
                DESCRIPTION
                "Valid port values are 1-32.  0 is the default indicating no test is active"
        ::= { adTA5kSplitStatusEntry 3 }
END
